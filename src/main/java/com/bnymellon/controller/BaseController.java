package com.bnymellon.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.bnymellon.bean.SendMail;

@SuppressWarnings("deprecation")
@Controller
@EnableWebMvc
public class BaseController {

	
	
	@RequestMapping(value="/",method= RequestMethod.GET)
	public ModelAndView welcome(ModelMap model){
		


		return new ModelAndView("index", "command", new SendMail());
		
	}
	
	
	@RequestMapping(value = "/mail/{id}")
    public  @ResponseBody  ResponseEntity<String> sendMail(@PathVariable(value="id") String id, ModelMap model) throws Exception {

		String subject="Time to book an uber!";
		String message="Time to book an uber!";
		
		
		Resource r=new ClassPathResource("applicationContext.xml");  
    	@SuppressWarnings("deprecation")
		BeanFactory b=new XmlBeanFactory(r);  
    	SendMail mail=(SendMail)b.getBean("mailMail"); 
    	
		mail.sendMail(id,subject, message);
		
		
        return new ResponseEntity<String>("Success", HttpStatus.OK);

	}
		
		
		
		
	
	

	@RequestMapping("/{startlatitude:.+}/{startlongitude:.+}/{destinationLatitude:.+}/{destinationLongitude:.+}")
    public  @ResponseBody  ResponseEntity<StringBuffer> welcomeName(@PathVariable("startlatitude") String startlatitude,@PathVariable("startlongitude") String startlongitude,@PathVariable("destinationLatitude") String destinationLatitude,@PathVariable("destinationLongitude") String destinationLongitude) throws Exception {


		String url="&origins="+startlatitude+","+startlongitude+"&destinations="+destinationLatitude+","+destinationLongitude+"&server_token=AIzaSyB6ky0s6kmaxH15hsxsNHKuZeI6n_OG2eA";
           
            URL obj = new URL("https","maps.googleapis.com","/maps/api/distancematrix/json?units=imperial"+url);
                   
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                    //add reuqest header
                    con.setRequestMethod("GET");
con.setRequestProperty("Content-Language", "en-US");

                   
                    // Send post request
                    con.setDoOutput(true);
                    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                    wr.flush();
                    wr.close();

                    int responseCode = ((HttpURLConnection) con).getResponseCode();

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                                    response.append(inputLine);
                    }
                    in.close();
                   
                                   
                   
                   
                   
                    return new ResponseEntity<StringBuffer>(response, HttpStatus.OK);

    }
	
	//uber api call
	@RequestMapping(value = "/{startlatitude:.+}/{startlongitude:.+}")
    public  @ResponseBody  ResponseEntity<StringBuffer> uberApiCall(@PathVariable("startlatitude") String startlatitude,@PathVariable("startlongitude") String startlongitude) throws Exception{

		


			String url="start_latitude="+startlatitude+"&start_longitude="+startlongitude+"&server_token=ECWcv5urK26d-pz-OHio9c9ovHpahx4UBbQIzMTi";
          
            URL obj = new URL("https","api.uber.com","/v1/estimates/time?"+url);
                   
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                    //add reuqest header
                    con.setRequestMethod("GET");

                   
                          
                  

                    int responseCode = ((HttpURLConnection) con).getResponseCode();

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                                    response.append(inputLine);
                    }
                    in.close();
                   
                                   
                   
                   
                   
                    return new ResponseEntity<StringBuffer>(response, HttpStatus.OK);

    }
	
	
	
}
