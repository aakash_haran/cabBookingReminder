package com.bnymellon.bean;  
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailSender;  
import org.springframework.mail.SimpleMailMessage;  
   
public class SendMail{  
    private MailSender mailSender;  
   
    public void setMailSender(MailSender mailSender) {  
        this.mailSender = mailSender;  
    }  
   
    public void sendMail(String to, String subject, String msg) {  
        //creating message  
    	
     
    	
        SimpleMailMessage message = new SimpleMailMessage();  
        message.setFrom("cabbooker1992@gmail.com");  
        message.setTo(to+".com");  
        message.setSubject(subject);  
        message.setText(msg);  
        //sending message  
        mailSender.send(message);     
    }  
}  