<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cab Booking Reminder</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script >
    var cabApp = angular.module("cabBookingApp",[]);
    cabApp.controller("cabBookingController",function($scope,$http,$interval){
      $scope.reminder= false;
      var trackApi=[];
      var rideTime,userMeetingTime,stop,autoTimer,globalTimeReminder,sourLat,sourLon,destLat,destLon;

        //validation for input time box
      $scope.validation=function(){
        $scope.displayError=null;
        if($scope.hour> 24 || $scope.min > 59){
          $scope.displayError="Please enter a valid hour/min"
        } else if($scope.hour == undefined && $scope.min == undefined){
          $scope.displayError="Please enter some value ";
        }
      };

        //function for user action for user action on remind me button
      $scope.remindMe=function(){
            $scope.sourceLat
        var date=new Date();
        var hours=0;
        var mins=0;
        globalTimeReminder=null;
        if($scope.hour)
          hours=$scope.hour;
        if($scope.min)
          mins=$scope.min;
        var minutes=date.setHours(hours);
        minutes=date.setMinutes(mins);
        userMeetingTime =new Date(minutes);
        if($scope.sourceLat && $scope.sourceLon && $scope.destinationLat && $scope.destinationLon){
        	if($scope.hour || $scope.min){
                if($scope.email){
                    googleMapsApiCall($scope.sourceLat,$scope.sourceLon,$scope.destinationLat,$scope.destinationLon);
                  }else {
                	  $scope.error="Please enter a valid email to schedule a reminder for booking a cab";
                  }

              }else{
                $scope.error="Please enter valid time to get a reminder";
              }
        }else{
        	$scope.error="Please enter all the co-ordinates";
        }
        
      };

        //google maps api call
      function googleMapsApiCall(sourceLat,sourceLon,destinationLat,destinationLon){
         var apiUrl="http://localhost:8080/cabBookingReminder/"+sourceLat+"/"+sourceLon+"/"+destinationLat+"/"+destinationLon;
        $http({ 
          url: apiUrl, 
          dataType: 'json', 
          method: 'POST', 
        }).then(function successCallback(response) {
          if(JSON.parse(response.data).rows[0]){
            if(JSON.parse(response.data).rows[0].elements[0].duration){
              var rideTime=JSON.parse(response.data).rows[0].elements[0].duration.text;
              trackApi.push(new Date()+ "Requested google maps api for "+$scope.email);
              console.log(trackApi);
              uberApiCall(rideTime);
            }else{
              $scope.error="There are zero results for your given co-ordinates or the given cordinates are incorrect"
            }
          }
          else{
            $scope.error="Either there are zero results or the given cordinates are incorrect"
          }
        }, function errorCallback(response) {
          $scope.error="Google maps api is down, Please try after after sometime";
        });
      }

        //uber api call
      function uberApiCall(rideTime){
        $http({ 
          url: "http://localhost:8080/cabBookingReminder/"+$scope.sourceLat+"/"+$scope.sourceLon, 
          dataType: 'json', 
          method: 'GET', 
        }).then(function successCallback(response) {
          var uberArray;
        console.log(response)
        if(response.data)
        {
          if(JSON.parse(response.data).times){
            var uberResponse=JSON.parse(response.data).times;
                if(uberResponse.length > 0){
                  for(var i=0;i<uberResponse.length;i++){
                      if(uberResponse[i].display_name === "uberGO"){
                        uberArray=uberResponse[i].estimate;
                        break;
                      }
                    
                        
                    }
                    var uberFinalTime=Math.floor(uberArray/60);
                    trackApi.push(new Date()+ "Requested uber api for "+$scope.email);
                    console.log(trackApi);
                    $scope.trackApi=trackApi;
                    if(uberFinalTime){
                    	 manipulation(rideTime,uberFinalTime);
                    }else{
                    	$scope.error= "Sorry there is no UberGo available now";
                    }
                }else{
                  $scope.error="Sorry there are no cabs available for your given latitude/longitude."
                }
          }else{
            $scope.error="Invalid Request, Please provide a correct latitude and longitude"
          }
        }
        }, function errorCallback(response) {
          $scope.error="Sorry, Uber api is down";
        });
      };

        //function to check whether it can  initiate the 15 min  autointerval call for uber and google api  
      function autoCheck(timeToSetremind){
        var tempTimeToSetRemind=timeToSetremind;
        var timeNow=new Date();
        var timeToRemindHours=tempTimeToSetRemind.getHours();
        var timeNowHours=timeNow.getHours();
        if((timeToRemindHours - timeNowHours) > 1){
          startAutoTimer(tempTimeToSetRemind);
        }else{
          $interval.cancel(autoTimer);
        }   
      }
      
        //autointerval timer for calling google and uber api
      function startAutoTimer(reminderTime){
        autoTimer=$interval(function(){
          var tempRemTime=reminderTime;
          if(tempRemTime <= globalTimeReminder){
            globalTimeReminder=tempRemTime;
            googleMapsApiCall($scope.sourceLat,$scope.sourceLon,$scope.destinationLat,$scope.destinationLon)
          }
            
      
        },15*60 * 1000)

      }
      

      function manipulation(rideTime,uberFinalTime){
        var googleMapsFinalTime;
        if(rideTime.search("mins")> -1 && rideTime.search("mins")< 6){
          var onlyMin;
          var onlyminError=rideTime.replace("mins","min");
          if(onlyminError){
            onlyMin=onlyminError.split("min");
          }else{
            onlyMin=rideTime.split("min");
          }
          googleMapsFinalTime=JSON.parse(onlyMin[0]);
        } else if(rideTime.search("hours")> -1){
          var hourMin = rideTime.replace("hours","hour");
            var hours;
            if(hourMin){
              hours=hourMin.split("hour");
            }else{
              hours=rideTime.split("hour");
            }
            if(hours){
              var minutes;
              var realHour=JSON.parse(hours[0])* 60;
              var minError=hours[1].replace("mins","min")
              if(minError){
                minutes=minError.split("min");
              }else{
                minutes=hours[1].split("min");
              }
              var realMin=parseInt(minutes[0]);
              googleMapsFinalTime=realHour+realMin;
            }
        }
        
        var uberGoogleMapsTime=googleMapsFinalTime +uberFinalTime;
        var uberGoogleFinalTimeStamp=new Date().setTime(new Date().getTime() + uberGoogleMapsTime*60000);
        var uberGoogleFinalTime =new Date(uberGoogleFinalTimeStamp);
        if(uberGoogleFinalTime <userMeetingTime){
        console.log("cab can be booked")
          var userMeetingHour=userMeetingTime.getHours();
          var userMeetingMin=userMeetingTime.getMinutes();
          var userMeetingFinal= (userMeetingHour*60)+userMeetingMin;
          var uberGoogleHour= uberGoogleFinalTime.getHours();
          var uberGoogleMin=uberGoogleFinalTime.getMinutes();
          var uberGoogleFinal=(uberGoogleHour*60)+uberGoogleMin;
          var diff= userMeetingFinal - uberGoogleFinal;

          var output=new Date().setTime(new Date().getTime() + diff*60000);
          var timeToremind= new Date(output);
          if(globalTimeReminder == null || globalTimeReminder == undefined || (timeToremind < globalTimeReminder))
            globalTimeReminder=timeToremind;
          $scope.error="Cab can be  booked and you will get a reminder around " + globalTimeReminder;
          startTimer(globalTimeReminder);
          autoCheck(timeToremind);

        }else if(uberGoogleFinalTime > userMeetingTime){
          $scope.error="Cab cannot be booked because it would take around " + uberGoogleFinalTime+ "which exceeds the destination time "+userMeetingTime+ " by you";
        }
      }
      function startTimer(response){
        stop=$interval(function(){
          var timenow=new Date().getHours()+new Date().getMinutes();
          var responseTime=response.getHours()+response.getMinutes();
          if (timenow == responseTime){
            $scope.error=false;
            $scope.reminder=true;
            $scope.notify=true;
            $scope.stop();
          }
        },100)

      }
      $scope.stop = function() {
    	  bookCab();
    	  $interval.cancel(stop);
        };
        $scope.$on('$destroy', function() {
            $scope.stop();
          });
      function bookCab(){
        $scope.notification="Time to book an uber!";
        sendMail();
      }
      function sendMail(){
        $http({ 
          url: "http://localhost:8080/cabBookingReminder/mail/"+$scope.email, 
          dataType: 'json', 
          method: 'GET', 
        }).then(function successCallback(response) {
        console.log(response)
        
        }, function errorCallback(response) {
          $scope.error="cab can be booked now but  mail cannot be sent due to some errors";
        });
      }
    });</script>
</head>
<body ng-app="cabBookingApp" ng-controller="cabBookingController">

<div class="jumbotron text-center">
  <h1>Cab Booking Reminder</h1>
</div>

<div ng-show="reminder" class="alert alert-success">
  <strong>Success!</strong>{{notification}}
  <button type="button" class="btn btn-primary" ng-click="reminder=false">close</button>
</div>

<div ng-show="error" class="alert alert-info">
  <strong>Info!</strong> {{error}}
  <button type="button" class="btn btn-primary" ng-click="error=false">close</button>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h3>Source Latitude</h3>
      <div class="form-group">
      <input type="text" class="form-control"  placeholder="Source Latitude" ng-model="sourceLat">
    </div>
    </div>
    <div class="col-sm-6">
      <h3>Source Longitude</h3>
      <div class="form-group">
      <input type="text" class="form-control"  placeholder="Source Longitude" ng-model="sourceLon">
    </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h3>Destination Latitude</h3>
      <div class="form-group">
      <input type="text" class="form-control"  placeholder="Destination Latitude" ng-model="destinationLat">
    </div>
    </div>
    <div class="col-sm-6">
      <h3>Destination Longitude</h3>
      <div class="form-group">
      <input type="text" class="form-control"  placeholder="Destination Longitude" ng-model="destinationLon">
    </div>
    </div>
  </div>
</div>
<div  class="alert alert-info">
  <strong>Info!</strong> Please enter the time in 24 hour format. Eg:Incase if you want to book a cab @7.30 p.m enter 19 hours 30 mins
</div>
<div ng-show="displayError" class="alert alert-danger">
  <strong>Error!</strong> {{displayError}}
  <button type="button" ng-click="displayError=false" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> close
      </button>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h3>Hours</h3>
      <div class="form-group">
      <input type="text" class="form-control"  onkeyup="if(/\D/g.test(this.value)) this.value= this.value.replace(/\D/g,'')" onclick="this.select()" ng-class="{error : hour > 12}" ng-change="validation()" min="0" max="12" ng-model="hour" placeholder="Enter Hours">
    </div>
    </div>
    <div class="col-sm-6">
      <h3>Minutes</h3>
      <div class="form-group">
      <input type="text" class="form-control" onkeyup="if(/\D/g.test(this.value)) this.value= this.value.replace(/\D/g,'')" onclick="this.select()" ng-class="{error : min > 60}" ng-change="validation()" min="0" max="60" ng-model="min" placeholder="Enter Minutes">
    </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <h3>Email Id</h3>
      <div class="form-group">
       <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" ng-model="email">
    </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-4">
 
    </div>
<div class="col-sm-4">
     <button type="button" class="btn btn-primary btn-lg" ng-click="remindMe() ">Remind Me</button>
    </div>
<div class="col-sm-4">
      
    </div>

  </div>
</div>
<div  class="alert alert-info" ng-repeat="track in trackApi  track by $index">
   {{track}}
</div> 


</body>
</html>

