
1) Get inputs from user for source latitude and source longitude.
2) get inputs from user for destination latitude and destination lomgitude.
3) get the time for the destination in 24 hour format.
4) Say the time is now 1.30 a.m and you want to attend a meeting at 6.00 a.m.
5)Call google maps api url for with query params as source latitude and source longitude and destination latitude and destination longitude which will return the time taken to reach the destination.(for example it returns 2 hours 30 min)
6)call uber api with source latitude and longitude as query params which will return the time taken by the uber cab to reach the user.
(for example it returns 1 hour)
7)So the total travel time for the user to reach the destination would be the summation of the above two return values(google api + uber api) . So in this case it is 3 hour 30 min.
8)For the first scenario, check whether the present time + 3 hour 30 min will be lesser than the destination time(6.00 a.m)
9)In this case it is lesser so the user can book a cab.
10)Now for thr reminder email time subtract the total travel time(3 hour 30 min ) from the destination time (6.00 a.m) which would yield the time for reminder which is 2.30 a.m.
11)So now the present time is 1.30 a.m and the reminder time would be around 2.30 a.m. We have got one hour to check whether we can book later than 2.30 a.m. So we have a interval timer which will ping the google maps api as well as uber api every 15 min to check whether the travel time cab be of less than what it previously had. If it is lesser than the previous one say it returns a total travel time of 3 hours only,then the reminder time to book uber cab will 3.00a.m now.A reminder email would be sent to the user email id at 3.00 a.m stating the user to book a cab.
12)If the reminder time is 1.45 a.m. The interval timer wont run. Obviously the reminder email will be sent to the user at 1.45 a.m stating the user to book a cab.