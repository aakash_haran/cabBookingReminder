<!DOCTYPE html>
<html>
  <head>
    <meta charset="ISO-8859-1">
    <title>Cab Booking Reminder</title>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script >
    var cabApp = angular.module("cabBookingApp",[]);
    cabApp.controller("cabBookingController",function($scope,$http,$interval){
    	$scope.reminder= false;
    	var rideTime;
    	var timeForReminder;
    	$scope.validation=function(){
    		$scope.displayError=null;
    		if($scope.time> 24 || $scope.min > 59){
    			$scope.displayError="Please enter a valid hour/min"
    		} else if($scope.time == undefined && $scope.min == undefined){
    			$scope.displayError="Please enter some value ";
    		}
    	};
    	$scope.button=function(){
    		var date=new Date();
    		var hours=0;
    		var minu=0;
    		if($scope.time)
    			hours=$scope.time;
    		if($scope.min)
    			minu=$scope.min;
    		var minutes=date.setHours(hours)
    		minutes=date.setMinutes(minu)
    		timeForReminder =new Date(minutes);
    		console.log(timeForReminder);
    		if($scope.time || $scope.min){
    			apiCall($scope.sourceLat,$scope.sourceLon,$scope.destinationLat,$scope.destinationLon);
    		}else {
    			$scope.error="Please enter correct time to get a reminder";
    		}

    	};
    	function apiCall(sourceLat,sourceLon,destinationLat,destinationLon){
    	var apiUrl="http://localhost:8080/HelloWorld/"+"units=imperial&origins="+sourceLat+","+sourceLon+"&destinations="+sourceLat+","+sourceLon+"&key=AIzaSyB6ky0s6kmaxH15hsxsNHKuZeI6n_OG2eA";
    		$http({ 
    			url: apiUrl, 
    			dataType: 'json', 
    			method: 'POST', 
    		}).then(function successCallback(response) {
    		console.log(response)
    			if(JSON.parse(response.data).rows[0]){
    				if(JSON.parse(response.data).rows[0].elements[0].duration){
    					var rideTime=JSON.parse(response.data).rows[0].elements[0].duration.text;
    					manipulation(rideTime);
    				}else{
    					$scope.error="There are zero results for your given co-ordinates or the given cordinates are incorrect"
    				}
    			}
    			else{
    				$scope.error="Either there are zero results or the given cordinates are incorrect"
    			}
    		}, function errorCallback(response) {
    			$scope.error="Google maps api is down, Please try after after sometime";
    		});
    	}
    	function manipulation(rideTime){
    		var hours=rideTime.split("hours");
    		if(hours){
    			var realHour=JSON.parse(hours[0])* 60;
    			var minutes=hours[1].split("mins");
    			var realMin=parseInt(minutes[0]);
    			var googleMapsFinalTime=realHour+realMin;
    		}else
    		{
    			var onlyMin=rideTime.split("mins");
    			googleMapsFinalTime=JSON.parse(onlyMin[0]);
    		}
    		var goUb=new Date().setTime(new Date().getTime() + googleMapsFinalTime*60000);
    		var timeTaken =new Date(goUb);
    		console.log(timeTaken);
    		if(timeTaken <timeForReminder){
    		console.log("cab can be booked")
    			var a=timeForReminder.getHours();
    			var b=timeForReminder.getMinutes();
    			var c= (a*60)+b;
    			var d= timeTaken.getHours();
    			var e=timeTaken.getMinutes();
    			var f=(d*60)+e;
    			var g= c -f;

    			var output=new Date().setTime(new Date().getTime() + g*60000);
    			var timeToremind= new Date(output);
    			console.log(timeToremind)
    			$scope.error="Cab is booked and you will get a reminder around " + timeToremind;
    			startTimer(timeToremind);

    		}else if(timeTaken > timeForReminder){
    			$scope.error="Cab cannot be booked because it would take around " + timeTaken+ "which exceeds the destination time "+timeForReminder+ " by you";
    		}
    	}
    	function startTimer(response){
    		$interval(function(){
    			var timenow=new Date().getHours()+new Date().getMinutes();
    			var responseTime=response.getHours()+response.getMinutes();
    			if (timenow == responseTime){
    				$scope.reminder=true;
    				$scope.notify=true;
    			}
    		},100)

    	}
    	$scope.$watch($scope.notify,function(newValue){
    		$scope.error=false;
    		$scope.notification="Its time to book cab now";
    	})
    });</script>
  </head>
  <body ng-app="cabBookingApp" ng-controller="cabBookingController">
  <div class="container">
      <div class="jumbotron">
        <h1>Cab Booking Reminder</h1>
      </div>
    </div>
    <div ng-show="reminder" class="container">
      <div class="jumbotron">
        <h1>{{notification}}</h1>
        <button type="button" class="btn btn-primary" ng-click="reminder=false">close</button>
      </div>
    </div>
    <div ng-show="error">
      <div class="jumbotron">
        <h3 class="bg-warning">{{error}}</h3>
        <button type="button" class="btn btn-primary" ng-click="error=false">close</button>
      </div>
    </div>
    <form class="form-inline">
      <div class="form-group">
        <label >Source Latitude</label>
      <input type="text" class="form-control"  placeholder="Source Latitude" ng-model="sourceLat">
      </div>
      <div class="form-group">
        <label >Source Longitude</label>
        <input type="text" class="form-control"  placeholder="Source Longitude" ng-model="sourceLon">
      </div>
    </form>
    <br>
    <form class="form-inline">
      <div class="form-group">
        <label >Destination Latitude</label>
        <input type="text" class="form-control"  placeholder="Destination Latitude" ng-model="destinationLat">
      </div>
      <div class="form-group">
        <label >Destination Longitude</label>
        <input type="text" class="form-control"  placeholder="Destination Longitude" ng-model="destinationLon">
      </div>
    </form>
    <br>
    <div ng-show="displayError" class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span>
      {{displayError}}
      <button type="button" ng-click="displayError=false" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> close
      </button>
    </div>
    <br>
    <p class="bg-info">Please enter the time in 24 hour format. Eg:Incase if you want to book a cab @7.30 p.m enter 19 hours 30 mins</p>
    <form class="form-inline">
      <div class="form-group">
        <label >Hours</label>
        <input type="text" class="form-control"  onkeyup="if(/\D/g.test(this.value)) this.value= this.value.replace(/\D/g,'')" onclick="this.select()" ng-class="{error : time > 12}" ng-change="validation()" min="0" max="12" ng-model="time" placeholder="Enter Hours">
      </div>
      <div class="form-group">
        <label >Minutes</label>
        <input type="text" class="form-control" onkeyup="if(/\D/g.test(this.value)) this.value= this.value.replace(/\D/g,'')" onclick="this.select()" ng-class="{error : min > 60}" ng-change="validation()" min="0" max="60" ng-model="min" placeholder="Enter Minutes">
      </div>
    </form>
	<br>
    <form class="form-inline">
      <div class="form-group">
        <label >Email address</label>
      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" ng-model="email">
      </div>
    </form>
    <button type="button" class="btn btn-primary" ng-click="button() ">Remind Me</button>
  </body>
</html>