var cabApp = angular.module("cabBookingApp",[]);
cabApp.controller("cabBookingController",function($scope,$http,$interval){
	$scope.reminder= false;
	var rideTime;
	var timeForReminder;
	$scope.validation=function(){
		$scope.displayError=null;
		if($scope.time> 24 || $scope.min > 59){
			$scope.displayError="Please enter a valid hour/min"
		} else if($scope.time == undefined && $scope.min == undefined){
			$scope.displayError="Please enter some value ";
		}
	};
	$scope.button=function(){
		var date=new Date();
		var hours=0;
		var minu=0;
		if($scope.time)
			hours=$scope.time;
		if($scope.min)
			minu=$scope.min;
		var minutes=date.setHours(hours)
		minutes=date.setMinutes(minu)
		timeForReminder =new Date(minutes);
		console.log(timeForReminder);
		if($scope.time || $scope.min){
			apiCall($scope.sourceLat,$scope.sourceLon,$scope.destinationLat,$scope.destinationLon);
		}else {
			$scope.error="Please enter correct time to get a reminder";
		}

	};
	function apiCall(sourceLat,sourceLon,destinationLat,destinationLon){
	var apiUrl="http://localhost:8080/HelloWorld/"+"units=imperial&origins="+sourceLat+","+sourceLon+"&destinations="+sourceLat+","+sourceLon+"&key=AIzaSyB6ky0s6kmaxH15hsxsNHKuZeI6n_OG2eA";
		$http({ 
			url: apiUrl, 
			dataType: 'json', 
			method: 'POST', 
		}).then(function successCallback(response) {
		console.log(response)
			if(JSON.parse(response.data).rows[0]){
				if(JSON.parse(response.data).rows[0].elements[0].duration){
					var rideTime=JSON.parse(response.data).rows[0].elements[0].duration.text;
					manipulation(rideTime);
				}else{
					$scope.error="There are zero results for your given co-ordinates or the given cordinates are incorrect"
				}
			}
			else{
				$scope.error="Either there are zero results or the given cordinates are incorrect"
			}
		}, function errorCallback(response) {
			$scope.error="Google maps api is down, Please try after after sometime";
		});
	}
	function manipulation(rideTime){
		var hours=rideTime.split("hours");
		if(hours){
			var realHour=JSON.parse(hours[0])* 60;
			var minutes=hours[1].split("mins");
			var realMin=parseInt(minutes[0]);
			var googleMapsFinalTime=realHour+realMin;
		}else
		{
			var onlyMin=rideTime.split("mins");
			googleMapsFinalTime=JSON.parse(onlyMin[0]);
		}
		var goUb=new Date().setTime(new Date().getTime() + googleMapsFinalTime*60000);
		var timeTaken =new Date(goUb);
		console.log(timeTaken);
		if(timeTaken <timeForReminder){
		console.log("cab can be booked")
			var a=timeForReminder.getHours();
			var b=timeForReminder.getMinutes();
			var c= (a*60)+b;
			var d= timeTaken.getHours();
			var e=timeTaken.getMinutes();
			var f=(d*60)+e;
			var g= c -f;

			var output=new Date().setTime(new Date().getTime() + g*60000);
			var timeToremind= new Date(output);
			console.log(timeToremind)
			$scope.error="Cab is booked and you will get a reminder around " + timeToremind;
			startTimer(timeToremind);

		}else if(timeTaken > timeForReminder){
			$scope.error="Cab cannot be booked because it would take around " + timeTaken+ "which exceeds the destination time "+timeForReminder+ " by you";
		}
	}
	function startTimer(response){
		$interval(function(){
			var timenow=new Date().getHours()+new Date().getMinutes();
			var responseTime=response.getHours()+response.getMinutes();
			if (timenow == responseTime){
				$scope.reminder=true;
				$scope.notify=true;
			}
		},100)

	}
	$scope.$watch($scope.notify,function(newValue){
		$scope.error=false;
		$scope.notification="Its time to book cab now";
	})
});